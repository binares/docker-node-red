DEV_CONTAINER=mynodered
DEV_IMAGE=mynodered
build:
	docker build --tag ${DEV_IMAGE} .

run: build
	docker run -v node_red_data:/data -p 1880:1880 -d --name $DEV_CONTAINER ${DEV_IMAGE}:latest

stop:
	docker stop $[DEV_CONTAINER}

rm: stop
	docker rm ${DEV_CONTAINER}

clean: stop rm

attach:
	docker exec -it ${DEV_CONTAINER} bash 

rebuild: stop rm build run attach 
