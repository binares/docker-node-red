FROM nodered/node-red
RUN npm install node-red-node-pi-gpiod
RUN npm install node-red-contrib-dht-sensor
RUN npm install --unsafe-perm node-red-contrib-dht-sensor
